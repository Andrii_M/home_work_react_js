import styled from "styled-components";

export const OutsideModal = styled.div`
  position: fixed;
  top: 0;
  left:0;
  width: 150vw;
  height: 150vh;
  background-color: rgba(57, 81, 88, 0.2);;
  z-index: 1;
`;

export const ModalContainer = styled.div`
  position: fixed;
  top: 40%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 500px;
  min-height: 200px;
  border-radius: 10px;
  z-index: 2;
  background-color: rgb(154, 170, 194);
  color: blue;
  font-family: 'Press Start 2P', cursive;
`;
export const ModalClose = styled.div`
  cursor: pointer;
  width: 20px;
  height: 20px;
`;
export const ModalHeader = styled.div`
  border-radius: 10px 10px 0 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 25px 25px;
  background-color: rgb(0 0 0 / 20%);

  h1 {
    margin: 0;
    font-size: 24px;
    font-weight: 400;
  }
`;

export const ModalMain = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 20px;
  text-align: center;
  min-height: 80px;

  p {
    margin: 0;
    font-size: 18px;
    font-weight: 500;
  }
`;

export const ModalFooter = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px 0px;
`;
