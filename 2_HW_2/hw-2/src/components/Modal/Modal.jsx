import { Component } from 'react';
import { ReactComponent as CloseLogo }  from './close.svg'
import PropTypes from 'prop-types';
import { ModalContainer,ModalHeader,ModalClose,ModalFooter,ModalMain,OutsideModal } from './style';

class Modal extends Component {

  render() {
    const {header,text,closeButton,onClose,children}= this.props;
    const btnClose = closeButton?<ModalClose><CloseLogo onClick={onClose} style={{width: "inherit",height: "inherit"}}/></ModalClose>:null;

    return (
    <>
      <OutsideModal onClick={onClose}/>
        <ModalContainer className='modal'>
            <ModalHeader>
                <h1>{header}</h1>
                {btnClose}
            </ModalHeader>
                <ModalMain>
                <p>{text}</p>
            </ModalMain>
            <ModalFooter>
                {children}
            </ModalFooter>
        </ModalContainer>
      </>
      )
  };
}

Modal.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string
};

Modal.defaultProps = {
  header: "Message",
  text: ""
};

export default Modal;
