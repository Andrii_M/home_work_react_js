import { Component } from "react";
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { faCartShopping } from '@fortawesome/free-solid-svg-icons';

class Header extends Component {

render() {
    const { arrayFavorite, arrayCart } = this.props;
    return (
        <header className="conteiner">
            <nav className="navigation">
                <h1 className="conteiner-text">Autotehniks</h1>
                <div className="conteiner-icon">
                    <cart className="conteiner-cart">
                        <FontAwesomeIcon icon={faCartShopping} />
                        <span className="cart">{arrayCart.length}</span>
                    </cart>
                    <favourite className="conteiner-favourite">
                        <FontAwesomeIcon icon={faHeart} />
                        <span className="favourite">{arrayFavorite.length}</span>
                    </favourite>
                </div>
            </nav>
        </header>
    )
}
}

Header.propTypes = {
    arrayFavorite: PropTypes.array,
    arrayCart: PropTypes.array
  };
  
  Header.defaultProps = {
    arrayFavorite: [],
    arrayCart: []
  };

export default Header;
