import { Component } from 'react';
import Card from '../product/Card'
import PropTypes from 'prop-types';

class ProductWrapper extends Component {

  render() {
    const { cardsArray, addToCart, addToFavorite } = this.props;
    let cards = null;
    if (Array.isArray(cardsArray)) {
      cards = cardsArray.map((product, ind) => {
        return <Card addToFavoriteFunc={addToFavorite} addToCartFunc={addToCart} key={ind} cardInfo={product} />
      });

    }
    return (
      <div className='card__wrapper'>
        {cards}
      </div>
    )
  };
}

ProductWrapper.propTypes = {
  cardsArray: PropTypes.array
};

ProductWrapper.defaultProps = {
  cardsArray: []
};

export default ProductWrapper;
