import { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as faStarSolid } from '@fortawesome/free-regular-svg-icons';
import PropTypes from 'prop-types';


class Checkbox extends Component {

  render() {
    const { isFavorite } = this.props;
    let iconStar;
    if (isFavorite) {
      iconStar = faStar;
    } else {
      iconStar = faStarSolid;
    }

    return (
      <div className='check-box'>
        <FontAwesomeIcon icon={iconStar} />
      </div>
    );
  }
}

Checkbox.propTypes = {
  isFavorite: PropTypes.bool
};

Checkbox.defaultProps = {
  isFavorite: false
};

export default Checkbox;
