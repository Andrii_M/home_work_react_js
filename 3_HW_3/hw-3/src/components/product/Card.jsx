import Button from '../button/Button.jsx';
import Checkbox from '../CheckBox/CheckBox.jsx';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
const CardProduct = ({ addToFavoriteFunc, cardInfo, showModal, isFavoriteProd, isInCart, showDeleteModal }) => {

  const changeFavorites = () => {
    addToFavoriteFunc(cardInfo, !isFavoriteProd);
  }
  const { productName, price, imgURL, productArticle, productColor } = cardInfo;
  return (
    <div className='conteiner-card'>
      <div className='conteiner-image'>
        {isInCart && <FontAwesomeIcon className='btn-close' icon={faXmark} onClick={() => showDeleteModal(cardInfo)} />}
        <img src={imgURL} alt="card img" />
      </div>
      {!isInCart ? (
        <div className='card-header' onClick={changeFavorites}>
          <h3 className="card__title">{productName}</h3>
          <Checkbox isFavorite={isFavoriteProd} />
        </div>
      )
        : (
          <div className='card-header'>
            <h3 style={{ cursor: "auto" }} className="card__title">{productName}</h3>
          </div>
        )
      }
      <div className='conteiner__card-desc'>
        <p className="card__desk-product">Product color: {productColor}</p>
        <p className="card__desk-product">Article: {productArticle}</p>
      </div>
      <div className='card-footer'>
        <p className='card-price'>{price} UAH</p>
        {isInCart ? <Button text="Купити" /> : <Button onClick={() => showModal(cardInfo)} text="add to cart" />}

      </div>
    </div>
  )
}

CardProduct.propTypes = {
  cardInfo: PropTypes.object,
  addToFavoriteFunc: PropTypes.func,
  addToCartFunc: PropTypes.func
};

CardProduct.defaultProps = {
  arrayCaddToFavoriteFuncart: null,
  addToCartFunc: null,
  isFavoriteProd: false,
  deleteCard: null,
  isInCart: false
};

export default CardProduct;