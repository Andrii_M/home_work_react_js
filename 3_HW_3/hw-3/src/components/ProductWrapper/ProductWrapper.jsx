import CardProduct from '../product/Card.jsx'
import PropTypes from 'prop-types';
const ProductWrapper = ({ cardsArray, showModal, addToFavorite }) => {
  let cards = null;
  let arrayFavorite = localStorage.getItem("FavoriteProd");
  if (Array.isArray(cardsArray)) {
    if (arrayFavorite) {
      arrayFavorite = JSON.parse(arrayFavorite);
      cards = cardsArray.map((product, ind) => {
        let sameElem = arrayFavorite.find(card => card.productArticle === product.productArticle);
        if (sameElem) {
          return <CardProduct addToFavoriteFunc={addToFavorite} showModal={showModal} key={ind} cardInfo={product} isFavoriteProd={true} />
        } else {
          return <CardProduct addToFavoriteFunc={addToFavorite} showModal={showModal} key={ind} cardInfo={product} />
        }
      });
    } else {
      cards = cardsArray.map((product, ind) => {
        return <CardProduct addToFavoriteFunc={addToFavorite} showModal={showModal} key={ind} cardInfo={product} />
      });
    }
  }
  return (
      <div className='card__wrapper'>
        {cards}
      </div>
  )
}

ProductWrapper.propTypes = {
  addToCart: PropTypes.func,
  addToFavorite: PropTypes.func
};

ProductWrapper.defaultProps = {
  addToCart: null,
  addToFavorite: null
};

export default ProductWrapper;