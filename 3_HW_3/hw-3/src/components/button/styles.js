import styled from "styled-components";
export const Btn = styled.div`
min-width: 80px;
border-radius: 6px;
border: 1px solid transparent;
cursor:pointer;
padding: 10px 10px;
text-align: center;
background-color: blue;
font-size: 16px;
text-transform: uppercase;
font-family: 'Press Start 2P', cursive;
color: white;
transition: 0.25s ease-in-out;
margin: 5px;

&:hover {
  border-color: yellowgreen;
  color: black;
  background-color: yellowgreen;
}
`