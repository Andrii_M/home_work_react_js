import Button from './button/Button';
import Modal from './modal/Modal';
import ProductWrapper from './ProductWrapper/ProductWrapper'; 
import CartFilling from './CartFilling/CartFilling'; 
import Favourite from './Favourite/Favourite'; 
import Header from './header/Header';
import { useState, useEffect } from 'react';
import * as React from 'react';
import { Routes, Route} from 'react-router-dom';
import './App.scss';

const App = () => {
  const [modalRender, setModalRender] = useState(false);
  const [modalDeleteRender, setmodalDeleteRender] = useState(false);
  const [chosenCard, setChosenCard] = useState(null);
  const [productArray, setProductArray] = useState(null);
  const [, setFavoriteProd] = useState(null);
  useEffect(() => {
    const url = process.env.PUBLIC_URL + "/product.json";
    fetch(url)
      .then(res => res.json())
      .then(data => {
        setProductArray(data);
      })
  }, [])


  const addToCart = () => {
    let arrayCart = localStorage.getItem("ProdInCart");
    if (arrayCart === null) {
      arrayCart = new Array(0);
    } else {
      arrayCart = JSON.parse(arrayCart);
      let sameElem = arrayCart.find(card => card.productArticle === chosenCard.productArticle);
      if (!sameElem) {
        arrayCart.push(chosenCard);
        localStorage.setItem("ProdInCart", JSON.stringify(arrayCart));
      }
    }
    setModalRender(false);
    setChosenCard(null);
  }
  const addToFavorites = (prod, isFavorite) => {
    let arrayFavorite = JSON.parse(localStorage.getItem("FavoriteProd"));
    let sameElem = arrayFavorite.find(card => card.productArticle === prod.productArticle);
    if (isFavorite && !sameElem) {
      arrayFavorite.push(prod)
    } else {
      arrayFavorite = arrayFavorite.filter(card => card.productArticle !== prod.productArticle)
    }
    localStorage.setItem("FavoriteProd", JSON.stringify(arrayFavorite));
    setFavoriteProd(arrayFavorite);
  }
  const closeModal = () => {
    setModalRender(false);
    setmodalDeleteRender(false);
    setChosenCard(null);
  }
  const showDeleteModal = (card) => {
    setChosenCard(card);
    setmodalDeleteRender(true);
  }
  const showModal = (card) => {
    setChosenCard(card);
    setModalRender(true);
  }
  const removeCard = () => {
    let arrayCart = JSON.parse(localStorage.getItem("ProdInCart"));
    let newArr = arrayCart.filter(card => card.productArticle !== chosenCard.productArticle);
    localStorage.setItem("ProdInCart", JSON.stringify(newArr));
    setmodalDeleteRender(false);
    setChosenCard(null);
  }
  let arrayFavorite = localStorage.getItem("FavoriteProd");
  let arrayCart = localStorage.getItem("ProdInCart");
  if (arrayFavorite === null) {
    arrayFavorite = new Array(0);
    localStorage.setItem("FavoriteProd", JSON.stringify(arrayFavorite));
  } else {
    arrayFavorite=JSON.parse(arrayFavorite);
  }
  if (arrayCart === null) {
    arrayCart = new Array(0);
    localStorage.setItem("ProdInCart", JSON.stringify(arrayCart));
  } else {
    arrayCart=JSON.parse(arrayCart);
  }
  let modalElement = null;
  let modalDeleteElement = null;
  if (modalRender) {
    modalElement = <Modal header="Повідомлення" text="Ви точно хочете додати товар до вашої корзини?" closeButton={true} onClose={closeModal}>
      <Button  text="OК" onClick={addToCart} />
      <Button text="Відмінити" onClick={closeModal} />
    </Modal>;
  }
  if (modalDeleteRender) {
    modalDeleteElement = <Modal header="Повідомлення" text="Ви дійсно хочете видалити продукт із корзини?" closeButton={true} onClose={closeModal}>
      <Button text="Видалити" onClick={removeCard} />
      <Button text="Відмінити" onClick={closeModal} />
    </Modal>;
  }

  return (
    <>
      {modalDeleteElement}
      {modalElement}
      <Header arrayFavorite={arrayFavorite} arrayCart={arrayCart} />
      <Routes>
        <Route path="/" element={<ProductWrapper showModal={showModal} addToFavorite={addToFavorites} cardsArray={productArray} />} />
        <Route path="Cart" element={<CartFilling addToFavorite={addToFavorites} cardsArray={arrayCart} showDeleteModal={showDeleteModal} />} />
        <Route path="Favorite" element={<Favourite showModal={showModal} addToFavorite={addToFavorites} cardsArray={arrayFavorite} />} />
        <Route path="*" element={<p style={{paddingTop: "300px", textAlign:'center',fontSize:'40px', color: 'white'}}>Помилка 404!</p>}>
        </Route>
      </Routes>
    </>
  );
}

export default App;
