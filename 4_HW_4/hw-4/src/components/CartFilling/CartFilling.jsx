import CardProduct from '../product/Card.jsx'
import { useSelector } from 'react-redux';
import {selectCartArray} from '../../store/selector';

const CartFilling = () => {
  const arrayCart = useSelector(selectCartArray);

    return (
      <div className='cart-filling'>
        {
           arrayCart.map((product, ind) => {
            return <CardProduct key={ind} cardInfo={product} isInCart={true} />
          })
        }
      </div>
    )
}

export default CartFilling;
