import { Btn } from './styles';
import PropTypes from 'prop-types';

const Button = ({ backgroundColor, text, onClick }) => {

    return (
      <Btn onClick={onClick} backgroundColor={backgroundColor}>
        {text}
      </Btn>
    )
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func
};

Button.defaultProps = {
  backgroundColor: "blue",
  text: "OK",
  onClick: null
};

export default Button;
