import CardProduct from '../product/Card.jsx';
import { useSelector } from 'react-redux';
import {selectFavoriteProd} from '../../store/selector';

const Favourite = () => {
  const favoriteProd = useSelector(selectFavoriteProd);
  return (
    <div className='favourite'>
      {
       favoriteProd && favoriteProd.map((product, ind) => {
          return <CardProduct key={ind} cardInfo={product} isFavoriteProd={true} isInFavorite={true}/>
        })
      }
    </div>
  )
}

export default Favourite;